import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

void main() => runApp(const WeatherApp());

class WeatherApp extends StatefulWidget {
  const WeatherApp({Key? key}) : super(key: key);

  @override
  State<WeatherApp> createState() => _WeatherAppState();
}

class _WeatherAppState extends State<WeatherApp> {
 int temperature = 0;
 String location = 'Thailand';
int woeid = 2487956;
  String weather = 'clear';
String abbreviation = '';
  List<String> abbreviationForecast = List.filled(7, '');
  String errorMessage = '';

String searchApiUrl = 'https://www.metaweather.com/api/location/search/?query=';
 String locationApiUrl = 'https://www.metaweather.com/api/location/';


 void fetchSearch(String input) async{
  var searchResult = await http.get(Uri.https(searchApiUrl + input));
   var result = json.decode(searchResult.body)[0];

setState(() {
        location = result["title"];
        woeid = result["woeid"];
        errorMessage = '';
      });
 }

 void fetchLocation() async {
    var locationResult = await http.get(Uri.https(locationApiUrl + woeid.toString()) );
    var result = json.decode(locationResult.body);
    var consolidated_weather = result["consolidated_weather"];
    var data = consolidated_weather[0];

    setState(() {
      temperature = data["the_temp"].round();
      weather = data["weather_state_name"].replaceAll(' ', '').toLowerCase();
      abbreviation = data["weather_state_abbr"];
    });
  }

  void onTextFieldSubmitted(String input) async {
    fetchSearch(input);
    fetchLocation();
    //fetchLocationDay();
  }
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Container(
        decoration:  BoxDecoration(
          image: DecorationImage(
            image: AssetImage("images/clear.png"),
            fit: BoxFit.cover,
          ),
        ),
        child:Scaffold(
          backgroundColor: Colors.transparent,
          body: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Column(
                children: <Widget>[
                  Center(
                child: Text(
                temperature.toString() + ' °C',
                style: const TextStyle(color: Colors.white, fontSize: 60.0),
              ),
              ),
              Center(
               child: Text(
                location,
                style: const TextStyle(color: Colors.white,fontSize: 40.0),
              ),
              ),
              ],
              ),
              
            ],
          ),
        ),
      ),
    );
  }
}


