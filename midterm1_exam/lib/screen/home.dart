import 'package:flutter/material.dart';
import 'package:midterm1_exam/screen/Login.dart';
import 'package:midterm1_exam/screen/calendar.dart';
import 'package:midterm1_exam/screen/timetable.dart';

class HomeScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Login"),
      ),
      body: Padding(
        padding: const EdgeInsets.fromLTRB(10, 50, 10, 0),
        child: SingleChildScrollView(
          child: Column(
            children: [
              Image.asset("assets/images/Buu-logo11.png",width: 200,height: 200,),
              const SizedBox(height: 20.0),    
              SizedBox(  
                         
                width: double.infinity,               
                child: ElevatedButton.icon(
                    icon: Icon(Icons.login),
                    label: Text("เข้าสู่ระบบ", style: TextStyle(fontSize: 20)),
                    onPressed: () {
                      Navigator.push(context, MaterialPageRoute(
                        builder: (context){
                          return LoginScreen();
                        }),
                      );
                    }),
              ),
              const SizedBox(height: 20.0),
              SizedBox(   
                            
                width: double.infinity,               
                child: ElevatedButton.icon(
                    icon: Icon(Icons.timeline),
                    label: Text("ตารางเรียนนิสิต", style: TextStyle(fontSize: 20)),
                    onPressed: (){
                      Navigator.push(context, MaterialPageRoute(
                        builder: (context){
                          return TimetableScreen();
                        }),
                      );
                    }
                    ),
              ),
              const SizedBox(height: 20.0),
              SizedBox(   
                            
                width: double.infinity,               
                child: ElevatedButton.icon(
                    icon: Icon(Icons.calendar_month),
                    label: Text("ปฏิทินการศึกษา", style: TextStyle(fontSize: 20)),
                    onPressed: (){
                      Navigator.push(context, MaterialPageRoute(
                        builder: (context){
                          return CalendarScreen();
                        }),
                      );
                    }
                    ),
              ),
              
            ],
          ),
        ),
      ),
    );
  }
}
